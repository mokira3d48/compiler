%{
	# include <stdlib.h>
	# include <stdio.h>

	void lexeme( void );
%}

%%

[-+*/] 			{ lexeme(); printf( "Ceci est un operateur de calcul arithmetique." ); }
F+			{ lexeme(); printf( "Une suite de 'F' a ete detectee !" ); }
"ab"{2,}		{ lexeme(); printf( "Deux 'ab' au moins a ete detectee !" ); }

[a-z|A-Z|0-9]{3} {
	lexeme();
	printf( "Un mot de longueur 3 a ete detectee !" ); 
	/* Par exemple:  ASD, eRT, 102, e3R, 4rT, ... */ 

}

"-"?[0-9]+ {
	/** Le nombre entier peut être négatif ou positif. Donc on note l'absence ou non du signe '-'. */
	lexeme();
	printf( "[]\tCeci est un nombre entier !" );

}

"-"?[0-9]+["."","][0-9]+ {
	/** 
	 * Le nombre réel peut être négatif ou positif.
	 * On doit détecter aussi le caractère ',' ou '.' en plus.
	 * Des nombres par la suite.
	 */
	lexeme();
	printf( "Un nombre reel a ete detecte !" );

}

[A-Z][A-Z|a-z|0-9]+ {
	/** On veut détecter des mots sous cette forme : Adf5, E45, DigitalVarchar91, ... */
	lexeme();
	printf( "Un identificateur valide a ete detecte !" );

}

[a-z|A-Z][A-Z|a-z|0-9]*("_"[A-Z|a-z|0-9]+)* {
	/** Ex : a, a3, aS, er_rt4, s_r, S_er3, ... */
	lexeme();
	printf( "Un second type d'identificateur a ete detecte !" );

}

.+ { printf( "Erreur de lexique !" ); /** Permet d'afficher "Erreur de lexique !" lorsque l'analyseur lexical ne va pas reconnaitre un mot. */ }

%%

/** Ici la fonction principale du compilateur. */
int main( void ) {
	printf( "Entrer de mot : \n" );
	yylex();

	return EXIT_SUCCESS;

}

/**
 * Cette fonction est appelée à la fin de l'interprétation lexicale.
 */
int yywrap( void ) {
	printf( "Fin de ligne de ficher detectee !\n" );
	return 1;

}

void lexeme( void ) {
	printf( "[OK]\t\"%s\" with %d characters\n", yytext, yyleng );
}







