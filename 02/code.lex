%{
	# include <stdlib.h>
	# include <stdio.h>

	void lexeme( void );

%}

letter		[a-z|A-Z]
number		[0-9]
alph_number	[a-z|A-Z|0-9]

ident		{letter}("_"?{alph_number}+)*
/** {letter}{alph_number}*("_"{alph_number}+)* est équivalent à {letter}("_"?{alph_number}+)* */



%%

'([^']|"''")*' {
	/**
	 * On a les '';
	 * On a entre [] tous sauf "'" OU on a le block de caractères : "''";
	 * La chaîne peut être vide ou pas vide.
	 */
	lexeme();
	printf( "Ceci est une chaine de caracteres en PASCAL." );
}

\"([^\"]|"\"\"")*\" {
	/**
	 * On peut tous avoir sauf les guillemet : "\"" ;
	 * La chaîne peut être vide ou pas vide.
	 */
	
	lexeme();
	printf( "Ceci est une chaine de caracteres en C." );
}

"%..".* {
	/**
	 * On doit avoir au début la chaîne "%.." ;
	 * On peut avoir tous '.' ou rien '*'.
	 */
	
	lexeme();
	printf( "Ceci est un commentaire en langage SNALL." );
}

"Snl_Int "{ident}(","(" ")*{ident})*" %." {
	/**
	 * PS : La déclaration de variable en langage SNALL ne peut pas normalement être analysée lexicalement.
	 * On doit avoir la chaîne "Snl_Int" suivis d'un " ";
	 * On doit avoir un identificateur valide {ident} déclaré précedement;
	 * On peut avoir une ou des ( "," suivi d'un " " puis d'un identificateur {ident} ) OU rien de tous ça;
	 * On doit avoir en fin la chaîne "%.".
	 */
	
	lexeme();
	printf( "Ceci est une declaration de variables en langage SNALL." );
}

.+ {
	printf( "[ERROR]\tSyntax non reconnu !" );
}

%%

int main( void ) {
	printf( "Enter a source code :\n" );
	yylex();

	return EXIT_SUCCESS;

}

int yywrap() {
	printf( "Fin de ligne attente !" );
	return 1;

}

void lexeme( void  ) {
	printf( "[OK]\t \" %s \" : %d characters []\n", yytext, yyleng );
	/** 
	 * {yytext} contient le terminal à récupérer;
	 * {yylong} contient la longueur du terminal récuperé.
	 */

}
