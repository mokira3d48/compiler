/*

En Flex, on commence par les eventuelles declarations C que l'on met entre les balises %{ et %}.
Entre ces balises, j'ai inclus la stdlib et la stdio de C. J'ai aussi declare 1 variable de type int.
La variable lineno correspond au numero de ligne.
La variable error est un booleen. Il est a true si une erreur est detectee.
Il existe la variable globale yylineno dans Flex que l'on peut ajouter en option. C'est cense gerer le numero de ligne tout seul mais pour des raisons inexpliquees, elle ne s'incremente pas chez moi.
Je prefere donc gerer le mecanisme de numero de ligne moi-meme. 

*/

%{

# include <stdlib.h>
# include <stdio.h>
# include <stdbool.h>

unsigned int lineno = 1;
bool         error  = false;


void lexeme(void);

%}

/*

Juste apres on declare les eventuelles variables Flex. Ce sont nos terminaux associes reconnus par des expressions regulieres.

*/

/* [[:digit:]] equivaut a [0-9] */

nombre 0|[1-9][[:digit:]]*

/* [[:alpha:]] equivaut a [a-zA-Z] (tout les caracteres de l'alphabet majuscules et minuscules) et [[:alnum:]] equivaut a [a-zA-Z0-9] (tout les caracteres alphanumeriques) */

variable [[:alpha:]][[:alnum:]]*

/*

Entre les %% on ecrit toutes les actions a chaque fois que l'analyseur detectera des lexemes (terminaux) de Simple.
On ecrit donc l'ensemble des terminaux de Simple.

*/

%%

{nombre} {
	printf("\tNombre trouve a la ligne %d. Il s'agit du nombre %s et comporte %d chiffre(s)\n",lineno,yytext,yyleng);
}

"afficher"	{lexeme();}

"="		{lexeme();}

"+"		{lexeme();}

"-"		{lexeme();}

"*"		{lexeme();}

"/"		{lexeme();}

"("		{lexeme();}

")"		{lexeme();}

"et"		{lexeme();}

"ou"		{lexeme();}

"non"		{lexeme();}

";"		{lexeme();printf("\n");}

"vrai"		{lexeme();}

"faux"		{lexeme();}

"\n"		{lineno++;}

 /* Si j'avais defini l'action de variable au debut, l'analyseur ne verrait plus les lexemes comme afficher ou supprimer mais comme des variables portant ce nom. L'ordre de definition des regles pour chaque lexemes n'est donc pas sans logique en Flex */ 

{variable} {
	printf("\tVariable trouvee a la ligne %d. Il s'agit de %s et comporte %d lettre(s)\n",lineno,yytext,yyleng);
}

 /* L'analyseur ne fait rien pour les espaces et tabulations */

" "|"\t" {}

 /* Le point est tout le reste qui n'a pas ete defini precedemment. Il est donc a mettre en dernier. */

. {
	fprintf( stderr, "\tERREUR : Lexeme inconnu a la ligne %d. Il s'agit de %s et comporte %d lettre(s)\n", lineno, yytext, yyleng );
	error = true;
}


%%

/*

J'ecris ici mes fonctions C apres le %%
Ma fonction main appellera la fonction de parsing yylex() qui sera construite a la compilation de la source Flex. C'est une fonction qui parse et detecte les lexemes (non terminaux) que nous avons defini dans notre programme Flex.
Le main n'est pas obligatoire. On peut utiliser la fonction main par defaut de Flex (qui ne fait qu'appeler yylex() seulement), il faut dans ce cas specifier main en option.

*/

int main(){
	printf("Debut de l'analyse lexicale :\n");
	yylex();
	printf("Fin de l'analyse !\n");
	printf("Resultat :\n");
	if(error){
		printf("\t-- Echec : Certains lexemes ne font pas partie du lexique du langage ! --\n");
	}
	else{
		printf("\t-- Succes ! --\n");
	}
	return EXIT_SUCCESS;
}

/*

La variable globale yytext contient le lexeme courant lu par l'analyseur.

*/

void lexeme(){
	printf("\tLexeme '%s' trouve a la ligne %d\n",yytext,lineno);
}

/*

La fonction yywrap() est appelee des que l'analyseur lexicale detecte le caractere EOF (End Of File). Elle doit retourner 1 pour mettre fin a l'analyse.
Cette fonction n'est pas obligatoire. On peut utiliser la fonction yywrap() par defaut de Flex (qui ne fait que retourner 1 juste), il faut dans ce cas specifier noyywrap en option. 

*/

int yywrap(){
	printf("\tFin de fichier detecte !\n");
	return 1;
}
